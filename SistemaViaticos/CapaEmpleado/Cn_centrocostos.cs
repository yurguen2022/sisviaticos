﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using backendDatos;

namespace CapaEmpleado
{
    public class Cn_centrocostos
    {
        private cr_centrogastos centro = new cr_centrogastos();

        public List<centrodecostos>Listarcentrocostos(){

            return centro.Listarcentrogastos();
            }

        public int registrarcentrocosto(centrodecostos obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el centro de costo es vacio";

            }
            if (string.IsNullOrEmpty(Mensaje))
            {
                return centro.Registrarcentrocostos(obj, out Mensaje);

            }
            else
            {
                return 0;
            }
        }

        public bool editarcentrocosto(centrodecostos obj, out String Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(obj.nombre) || string.IsNullOrWhiteSpace(obj.nombre))
            {
                Mensaje = "el centro de costos es vacio";

            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return centro.editarcentrocosto(obj, out Mensaje);
            }
            else
            {
                return false;
            }
        }
        public bool Elimarcentrocosto(int id, out string Mensaje)
        {
            return centro.eliminarcnentrocosto(id, out Mensaje);
        }

    }
}
