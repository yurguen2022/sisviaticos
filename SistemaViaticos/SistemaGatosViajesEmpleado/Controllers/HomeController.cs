﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CapaEntidad;
using CapaEmpleado;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;

namespace SistemaGatosViajesEmpleado.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

       public int idm ;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Misviajes()
            
        {
            
            
            return View();
        }

        public ActionResult MisComprobantes()

        {


            return View();
        }

        public ActionResult AgregarComprobante()

        {


            return View();

        }
        [HttpPost]
        
        public ActionResult Misvia(int id)
        {
            
            Session["idcuen"]=id;

            idm = id;

            return Json(new { data = id }, JsonRequestBehavior.AllowGet);
        }



      

        public ActionResult SolictarViaje()
        {


            return View();
        }
        [HttpGet]

        public JsonResult listausuario()
        {
            List<usuarios> olista = new List<usuarios>();
            olista = new CA_usuario().Listarusua();
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listaproyecto()
        {
            List<proyecto> olista = new List<proyecto>();
            olista = new Cn_proyecto().Listarproyecto2();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]

        public JsonResult listauniadmin()
        {
            List<uniAdmin> olista = new List<uniAdmin>();
            olista = new Cn_unidadmin().Listaruniadmin();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]

        public JsonResult listacentrogasto()
        {
            List<centrodecostos> olista = new List<centrodecostos>();
            olista = new Cn_centrocostos().Listarcentrocostos();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listarviaje()
        {
            List<solicitarviaje> olista = new List<solicitarviaje>();
            olista = new cn_solictarviaje().Listar2();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listarviajeus()
        {
            int id = Convert.ToInt32(Session["idusuario"]);
            List<solicitarviaje> olista = new List<solicitarviaje>();
            olista = new cn_solictarviaje().Listarporusua(id);


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]



        public ActionResult ImporteViaje(int id)
        {

            Session["importe"] = id;

            //  idm = id;

            return Json(new { data = id }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult sumarcompro()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);


            total olista = new Cn_comprobantegasto().sumarcompro(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult guardarviajes(solicitarviaje objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.idsolicitud == 0)

            {
                resultado = new cn_solictarviaje().Registrar(objeto, out Mensaje);


            }
            else
            {
                resultado = new cn_solictarviaje().editarsolicitarviajes(objeto, out Mensaje);

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult guardarcontableempleado(contableempleado objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
            if (objeto.id== 0)

            {
                resultado = new cn_contableempleado().Registrarcontableempleado(objeto, out Mensaje);


            }
            else
            {
                resultado = false;

            }
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult editandoimporte(solicitarviaje objeto)

        {

            object resultado;
            string Mensaje = string.Empty;
           
                resultado = new cn_solictarviaje().editaimpo(objeto, out Mensaje);


          
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]

        public JsonResult listacuentascontables()
        {

            List<cuentas_contables> olista = new List<cuentas_contables>();
            olista = new Cn_cuentascontables().Listarcuentascontables();


            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        #region

        [HttpGet]

        public JsonResult listacontaemple()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            List<contableempleado> olista = new List<contableempleado>();
            olista = new cn_contableempleado().Listarcontaemple(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public JsonResult listacontables()
        {
            int ido;

            ido = Convert.ToInt16(Session["idcuen"]);

            
         total olista = new cn_contableempleado().Listitacontable(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult editacontable(contableempleado objeto)
        {

            object resultado;
            string Mensaje = string.Empty;
          
                resultado = new cn_contableempleado().editarcuentascontables(objeto, out Mensaje);

            
            return Json(new { resultado = resultado, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }



        #endregion
        [HttpPost]
        public JsonResult eliminarcoempleado(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new cn_contableempleado().Elimarcuentascontables(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }

        //comprobantegasto

        [HttpGet]

        public JsonResult listacomdegasto()
        {
            int ido;

          ido= Convert.ToInt16(Session["idcuen"]);

            List<comprobante_gasto> olista = new List<comprobante_gasto>();
            olista = new Cn_comprobantegasto().Listacomprobante(ido);
            return Json(new { data = olista }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult guardarcomprobanti(string  objeto, HttpPostedFileBase archivoimagen)

        {

            
            string Mensaje = string.Empty;
            bool operacionexitosa = true;
            bool guardarimagenexito = true;

            comprobante_gasto com = new comprobante_gasto();
            com = JsonConvert.DeserializeObject<comprobante_gasto>(objeto);

             




            if (com.id == 0)

            {
                int idgenerado = new Cn_comprobantegasto().Registrarcomgastos(com, out Mensaje);
                if (idgenerado != 0)
                {
                    com.id = idgenerado;
                }
                else
                {
                    operacionexitosa = false;
                }

            }
            else
            {
                operacionexitosa = new Cn_comprobantegasto().editarcomprobantegasto(com, out Mensaje);

            }

            if (operacionexitosa)
            {
                if (archivoimagen != null)
                {
                    string rutaimagen = ConfigurationManager.AppSettings["servidordefotos"];
                    string extension = Path.GetExtension(archivoimagen.FileName);
                    string nombre_imagen = string.Concat(com.id.ToString(), extension);

                    try
                    {
                        archivoimagen.SaveAs(Path.Combine(rutaimagen, nombre_imagen));
                    }catch (Exception ex)
                    {
                        string msg = ex.Message;
                        guardarimagenexito = false;
                    }

                    if (guardarimagenexito)
                    {
                        com.rutaimagen = rutaimagen;
                        com.nombreimagen = nombre_imagen;

                        bool res = new Cn_comprobantegasto().guardardatosimagen(com, out Mensaje);
                    }
                    else
                    {
                        Mensaje = "hubo problemas con la imagen";
                    }
                }
            }


            return Json(new { operacionexitosa = operacionexitosa,idgenerado=com.id, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]

        public JsonResult imagencomprobante(int id)
        {
            bool conversion;

            comprobante_gasto co = new Cn_comprobantegasto().Listarimagen().Where(p => p.id == id).FirstOrDefault();
            string textoBase64 = Cn_recursos.convertirBase64(Path.Combine(co.rutaimagen,co.nombreimagen),out conversion);


            return Json(new
            {
                conversion = conversion,
                textoBase64 = textoBase64,
                extension = Path.GetExtension(co.nombreimagen)
            },
            JsonRequestBehavior.AllowGet
            );





        }

        [HttpPost]
        public JsonResult eliminarcom(int id)
        {
            bool respuesta = false;
            string Mensaje = string.Empty;
            respuesta = new Cn_comprobantegasto().Elimarcomprobantegas(id, out Mensaje);

            return Json(new { resultado = respuesta, Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);


        }



    }
}